﻿using car_rental.App;
using Xunit;
using System;
using Moq;

namespace car_rental.Test
{
    public class RentalUnitTests : IDisposable
    {
        Mock<CombiCar> mock;
        int distanceTraveled;
        int daysPassed;

        public RentalUnitTests()
        {
            mock = new Mock<CombiCar>("123");

            mock.Setup(car => car
            .CalculatePrice(It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<int>(), It.IsAny<int>())
            )
            .Returns(10)
            .Callback((decimal baseP, decimal baseKmP, int distance, int days) =>
            {
                distanceTraveled = distance;
                daysPassed = days;
            });
        }

        public void Dispose()
        {
            distanceTraveled = 0;
            daysPassed = 0;
        }

        [Fact]
        public void DoNotAllowEndDistanceToBeLowerValueThanStartDistance()
        {
            var rental = new Rental(100, 10, mock.Object, "123", DateTime.Parse("2017-06-10"), 1000, "foo");

            Exception ex = Assert.Throws<Exception>(() => rental.DistanceEnd = 500);
            Assert.Equal("Distance end must be greater than distance at start (1000km) ", ex.Message);

        }

        [Fact]
        public void DoNotAllowRentEndToBeEarlierThanRentStart()
        {
            var rental = new Rental(100, 10, mock.Object, "123", DateTime.Parse("2017-06-10"), 1000, "foo");

            Exception ex = Assert.Throws<Exception>(() => rental.RentEnd = DateTime.Parse("2017-06-8"));
            Assert.Equal("RentEnd must be after the rent started, 2017-06-10, 00:00", ex.Message);
        }

        [Fact]
        public void DaysAndDistancePassedToCarCalculation()
        {
            var rental = new Rental(100, 10, mock.Object, "123", DateTime.Parse("2017-06-10"), 1000, "foo");
            rental.RentEnd = DateTime.Parse("2017-06-12");
            rental.DistanceEnd = 1050;

            rental.GetPrice();
            Assert.Equal(2, daysPassed);
            Assert.Equal(50, distanceTraveled);
        }


        [Fact]
        public void DaysRoundedUpwards()
        {
            var rental = new Rental(100, 10, mock.Object, "123", DateTime.Parse("2017-06-10, 12:00"), 1000, "foo");
            rental.RentEnd = DateTime.Parse("2017-06-12, 13:00");
            rental.DistanceEnd = 1050;

            rental.GetPrice();
            Assert.Equal(3, daysPassed);
            Assert.Equal(50, distanceTraveled);
        }
    }
}