﻿using car_rental.App;
using Xunit;

namespace car_rental.Test
{
    public class CarUnitTests
    {
        [Fact]
        public void SmallCarPriceCalculation()
        {
            var smallCar = new SmallCar("123");
            var price = smallCar.CalculatePrice(10, 0, 0, 4);
            Assert.Equal(40m, price);
        }

        [Fact]
        public void CombiCarPriceCalculation()
        {
            var combi = new CombiCar("123");
            var price = combi.CalculatePrice(10, 3, 15, 4);

            Assert.Equal(97m, price);
        }

        [Fact]
        public void TruckCarPriceCalculation()
        {
            var combi = new TruckCar("123");
            var price = combi.CalculatePrice(10, 3, 15, 4);

            Assert.Equal(127.5m, price);
        }
    }
}
