Car rental program!
===================
A simple console application for handling car rentals.
Run via Visual Studio or run `dotnet run` in the project folder.

Features
-----
You can start a rental, and end a rental. Upon ending a rental you will get a price based on type of car, how many started days and how far the car has traveled.
When exiting the application it will automatically remove any trace of having existed and any changes made will dissapear. As you would want to.


Tests
-------------
To run the tests do `dotnet xuint` from the tests project folder.

