﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_rental.App
{
    public class StateManager
    {
        private static StateManager inputManager = null;
        private Dictionary<string, Rental> rentals;
        private decimal baseDayPrice;
        private decimal baseKmPrice;

        private StateManager()
        {
            this.rentals = new Dictionary<string, Rental>();
            this.baseDayPrice = 100m;
            this.baseKmPrice = 100m;
            SeedData(rentals);
        }

        public static StateManager GetInstance()
        {
            if (StateManager.inputManager == null)
            {
                StateManager.inputManager = new StateManager();
            }
            return StateManager.inputManager;
        }

        public Dictionary<string, Rental> Rentals
        {
            get => rentals;
            private set => rentals = value;
        }

        public void AddRental(string bookingId, Car car, string customerPnr, DateTime startDate, int distanceStart)
        {
            var rental = new Rental(baseDayPrice, baseKmPrice, car, bookingId, startDate, distanceStart, customerPnr);
            this.rentals[bookingId] = rental;
        }

        public Rental GetRental(string bookingId)
        {
            Rental rental;
            try
            {
                rental = rentals[bookingId];
            }
            catch (KeyNotFoundException)
            {  
                return null;
            }
            return rental;
        }

        private void SeedData(Dictionary<string, Rental> rentals)
        {
            for (var i = 0; i < 5; i++)
            {
                Car car;
                if (i % 2 == 0)
                {
                    car = new SmallCar("abc-" + i + i + i);
                }
                else if (i % 3 == 0)
                {
                    car = new CombiCar("abc-" + i + i + i);
                }
                else
                {
                    car = new TruckCar("abc-" + i + i + i);
                }
                var bookingId = "abc" + i;
                var rental = new Rental(baseDayPrice, baseKmPrice, car, bookingId, DateTime.Parse("2017-06-10, 12:00"), 1000, "mockpnr");
                rentals.Add(bookingId, rental);
            }

            var rentalEnded = new Rental(baseDayPrice, baseKmPrice, new CombiCar("foo-321"), "Foo123", DateTime.Parse("2017-06-10, 12:00"), 1000, "mockpnr");
            rentalEnded.DistanceEnd = 1025;
            rentalEnded.RentEnd = DateTime.Parse("2017-06-13, 12:00");
            rentals.Add("Foo123", rentalEnded);
        }
    }
}
