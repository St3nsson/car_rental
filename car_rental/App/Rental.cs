﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_rental
{
    public class Rental
    {
        private DateTime rentStart;
        private DateTime? rentEnd;

        private int distanceStart;
        private int distanceEnd;

        private Car car;
        private decimal baseDayPrice;
        private decimal baseKmPrice;
        private string bookingId;
        private string renterPNR;

        public Rental(decimal baseDayPrice, decimal baseKmPrice, Car car, string bookingId, DateTime rentStart, int distanceStart, string renterPNR)
        {
            this.car = car;
            this.bookingId = bookingId;
            this.baseDayPrice = baseDayPrice;
            this.baseKmPrice = baseKmPrice;
            this.rentStart = rentStart;
            this.distanceStart = distanceStart;
            this.renterPNR = renterPNR;
        }

        public DateTime RentStart
        {
            get { return this.rentStart; }
            set { this.rentStart = value; }
        }
        public DateTime? RentEnd
        {
            get { return this.rentEnd; }
            set
            {
                if (RentStart == null)
                {
                    throw new Exception("Start of rent must be set before ending rent");

                }
                else if (this.RentStart.CompareTo(value.Value) >= 0)
                {
                    throw new Exception(String.Format("RentEnd must be after the rent started, {0:yyyy-MM-dd, HH:mm}", this.RentStart));
                }
                this.rentEnd = value;
            }
        }
        public int DistanceStart
        {
            get { return this.distanceStart; }
            set { this.distanceStart = value; }
        }
        public int DistanceEnd
        {
            get { return this.distanceEnd; }
            set
            {
                if (this.DistanceStart > value)
                {
                    throw new Exception(String.Format("Distance end must be greater than distance at start ({0}km) ", this.DistanceStart));
                }
                this.distanceEnd = value;
            }
        }

        public string BookingId { get => bookingId; set => bookingId = value; }

        public decimal GetPrice()
        {
            var distanceTraveled = this.DistanceEnd - this.DistanceStart;
            var timeRented = this.RentEnd.Value.Subtract(this.RentStart);
            var daysPassed = timeRented.Days;
            if(timeRented.Hours > 0)
            {
                daysPassed++;
            }
            return car.CalculatePrice(this.baseDayPrice, this.baseKmPrice, distanceTraveled, daysPassed);
        }

        public bool RentalHasEnded()
        {
            return this.RentEnd != null;
        }

        public override string ToString()
        {
            string str;
            var rentStartedStr = String.Format("Rent Started: {0: yyyy-MM-dd, HH:mm}.", this.RentStart);
            var typeOfCarStr = String.Format("Type of Car: {0}.", this.car.CarType);
            var bookingIdStr = String.Format("Booking id: {0}.", this.bookingId);
            var licenePlateStr = String.Format("Car licence plate: {0}.", this.car.LicencePlate);
            var distanceStartStr = String.Format("Car travled at start of rent: {0}km.", this.DistanceStart);

            if (this.RentalHasEnded())
            {
                var priceStr = String.Format("Price of rental: {0:C}", this.GetPrice());
                var rentEndedStr = String.Format("Rent Started: {0: yyyy-MM-dd, HH:mm}.", this.RentEnd);
                var distanceEndStr = String.Format("Car travled at end of rent: {0}km.", this.DistanceEnd);
                var distanceTravledStr = String.Format("Distance travled: {0}km.", this.DistanceEnd - this.DistanceStart);
                str = String.Format("{0} \n{1} \n{2} \n{3} \n{4} \n{5} \n{6} \n{7} \n{8}", bookingIdStr, typeOfCarStr, licenePlateStr, rentStartedStr, rentEndedStr, distanceStartStr, distanceEndStr, distanceTravledStr, priceStr);
            }
            else
            {
                str = String.Format("{0} \n{1} \n{2} \n{3} \n{4}", bookingIdStr, typeOfCarStr, licenePlateStr, rentStartedStr, distanceStartStr);
            }

            return str;
        }
    }
}
