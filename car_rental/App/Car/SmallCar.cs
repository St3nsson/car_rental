﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_rental.App
{
    public class SmallCar : Car
    {

        public SmallCar(string licencePlate) : base(licencePlate)
        {
            this.carType = "Small Car";
        }

        public override decimal CalculatePrice(decimal baseDayPrice, decimal baseKmPrice, int distanceTravled, int daysPassed)
        {
            return baseDayPrice * daysPassed;
        }
    }
}
