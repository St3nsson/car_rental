﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_rental.App
{
    public class TruckCar : Car
    {
        public TruckCar(string licencePlate) : base(licencePlate)
        {
            this.carType = "Truck";
        }

        public override decimal CalculatePrice(decimal baseDayPrice, decimal baseKmPrice, int distanceTravled, int daysPassed)
        {
            return baseDayPrice * daysPassed * 1.5m + baseKmPrice * distanceTravled * 1.5m;
        }
    }
}
