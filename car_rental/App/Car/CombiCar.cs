﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_rental.App
{
    public class CombiCar : Car
    {
        public CombiCar(string licencePlate) : base(licencePlate)
        {
            this.carType = "Combi Car";
        }
        public override decimal CalculatePrice(decimal baseDayPrice, decimal baseKmPrice, int distanceTravled, int daysPassed)
        {
            return baseDayPrice * daysPassed * 1.3m + baseKmPrice * distanceTravled;
        }
    }
}
