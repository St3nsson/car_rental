﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_rental
{
    public abstract class Car
    {
        private string licencePlate;
        protected string carType;
        protected Car(string licencePlate)
        {
            this.licencePlate = licencePlate;
        }
        public abstract decimal CalculatePrice(decimal baseDayPrice, decimal baseKmPrice, int distanceTraveled, int daysPassed);

        public string LicencePlate
        {
            get { return this.licencePlate; }
        }

        public string CarType
        {
            get { return this.carType; }
        }
    }
}
