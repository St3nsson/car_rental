﻿using System;

namespace car_rental.App
{
    public class InputManager
    {
        private static InputManager inputManager = null;
        private StateManager stateManager;

        private InputManager(StateManager stateManager)
        {
            this.stateManager = stateManager;
        }

        public static InputManager GetInstance()
        {
            if(InputManager.inputManager == null)
            {
                InputManager.inputManager = new InputManager(StateManager.GetInstance());
            }
            return InputManager.inputManager;
        }

        public int MainOption()
        {
            Console.WriteLine("Please proceed with one of three options");
            Console.WriteLine("Start a new rental [1]. End a rental[2]. List active rentals[3]. List ended rentals[4]. Exit Application and obviously remove all rentals[5]");
            var input = Console.ReadLine();
            int mainOption;
            Int32.TryParse(input, out mainOption);

            return mainOption;
        }

        public string ReadBookingId()
        {
            Console.WriteLine("Enter A booking id");
            var bookingId = Console.ReadLine();
            return bookingId;
        }

        public string ReadLicencePlate()
        {
            Console.WriteLine("Enter licence plate");
            var licencePlate = Console.ReadLine();
            return licencePlate;
        }

        public string ReadPnr()
        {
            Console.WriteLine("Enter customer person number");
            var customerPNR = Console.ReadLine();
            return customerPNR;
        }

        public Car ReadCar(string licencePlate)
        {
            Car car;
            int carType;
            do
            {
                Console.WriteLine("What type of car is it? Small[1], Combi[2] or Truck[3]?");
                var carTypeInput = Console.ReadLine();
                Int32.TryParse(carTypeInput, out carType);
                switch (carType)
                {
                    case 1: car = new SmallCar(licencePlate); break;
                    case 2: car = new CombiCar(licencePlate); break;
                    case 3: car = new TruckCar(licencePlate); break;
                    default: car = null; break;
                }
            } while (car == null);
            return car;
        }

        public DateTime ReadDateInput(string text)
        {
            string dateInput;
            DateTime? date = null;
            do
            {
                Console.WriteLine(text);
                dateInput = Console.ReadLine();

                try
                {
                    date = DateTime.Parse(dateInput);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid Date format. Try Again.");
                    date = null;
                }

            } while (date == null);

            return date.Value;
        }

        public int ReadDistanceInput(string text)
        {
            int distance = 0;
            do
            {
                Console.WriteLine(text);
                var distanceStartInput = Console.ReadLine();

                if (!Int32.TryParse(distanceStartInput, out distance))
                {
                    Console.WriteLine("Input must be a number");
                };
            } while (distance == 0);
            return distance;
        }
    }
}
