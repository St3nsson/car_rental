﻿using car_rental.App;
using System.Collections.Generic;

namespace car_rental
{
    class Program
    {
        static void Main(string[] args)
        {
            var carRental = new CarRentalProgram(InputManager.GetInstance(), StateManager.GetInstance());
            carRental.Start();
        }
    }
}