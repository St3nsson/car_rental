﻿using System;
using System.Linq;

namespace car_rental.App
{
    public class CarRentalProgram
    {
        private InputManager inputManager;
        private StateManager stateManager;

        public CarRentalProgram(InputManager inputManager, StateManager stateManager)
        {
            this.inputManager = inputManager;
            this.stateManager = stateManager;
        }

        public void Start()
        {
            int mainOption = inputManager.MainOption();
            while(mainOption != 5){
                switch (mainOption)
                {
                    case 1: StartRental(); break;
                    case 2: EndRental(); break;
                    case 3: ListActiveRentals(); break;
                    case 4: ListEndedRentals(); break;
                }
                mainOption = inputManager.MainOption();
            }
        }

        private void ListEndedRentals()
        {
            var activeRentals = stateManager.Rentals.Values.Where(r => r.RentalHasEnded());
            foreach (Rental r in activeRentals)
            {
                Console.WriteLine(r.ToString());
                Console.WriteLine("----------");
            }
        }

        private void ListActiveRentals()
        {
            var activeRentals = stateManager.Rentals.Values.Where(r => !r.RentalHasEnded());
            foreach (Rental r in activeRentals)
            {
                Console.WriteLine(r.ToString());
                Console.WriteLine("----------");
            }
        }

        private void EndRental()
        {
            string bookingId;
            DateTime endDate;
            Rental rental;
            Console.WriteLine("Ending Rental");
            bookingId = inputManager.ReadBookingId();
            rental = stateManager.GetRental(bookingId);
            if(rental == null)
            {
                Console.WriteLine("Booking with that OrderId doesn't exist");
                return;
            }
            if (rental.RentalHasEnded())
            {
                Console.WriteLine("This rental has already ended and it's price was: {0:C}", rental.GetPrice());
                return;
            }
            var dateInputValid = false;
            while (!dateInputValid)
            {
                endDate = inputManager.ReadDateInput("Enter end date and time for rental (YYYY-MM-DD, HH:mm)");
                try
                {
                    rental.RentEnd = endDate;
                    dateInputValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    dateInputValid = false;
                }
            }
            var distanceInputValid = false;
            while (!distanceInputValid)
            {
                var distance = inputManager.ReadDistanceInput("Enter car's current traveled distance.");

                try
                {
                    rental.DistanceEnd = distance;
                    distanceInputValid = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    distanceInputValid = false;
                }
            }
            var price = rental.GetPrice();
            Console.WriteLine("Rent was successfully ended. Price total of {0:C}", price);
        }

        private void StartRental()
        {
            string bookingId;
            string licenceplate;
            string customerPnr;
            Car car;
            DateTime startDate;
            int distanceStart;
            Console.WriteLine("Starting rental");
            bookingId = inputManager.ReadBookingId();
            if(stateManager.Rentals.ContainsKey(bookingId))
            {
                Console.WriteLine("Booking with that Id already exist. Only one rental per booking is allowed.");
                return;
            }

            licenceplate = inputManager.ReadLicencePlate();
            car = inputManager.ReadCar(licenceplate);
            customerPnr = inputManager.ReadPnr();
            startDate = inputManager.ReadDateInput("Enter start date and time for rental (YYYY-MM-DD, HH:mm)");
            distanceStart = inputManager.ReadDistanceInput("Enter car's current traveled distance.");
            stateManager.AddRental(bookingId, car, customerPnr, startDate, distanceStart);
        }
    }
}
